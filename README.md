# Backend-Test - API
Nodejs, Expressjs and MongoDB based api

## Installation

**Note**  
You need to have [Node.js](http://nodejs.org/) installed on your machine.  
You need to have MongoDB key to connect to a database.

## 1) Clone the repository, install node packages



`git clone https://bitbucket.org/saqib054/test-backend.git`  
`cd test-backend`  
`npm install`  

## Starting server

Setup .env file with followind credentials
  
`
    mongoURI: 'mongodb-Key'
`

`Run npm start`  
Your app should now be running on [localhost:5000](http://localhost:5000/).

## Tests

`npm test`

## Manual Testing  
**Open postaman and try accessing**

Add Customer

http://localhost:5000/api/add-customer 

Get customer list  
http://localhost:5000/api/customer-list   

Get one Customer  
http://localhost:5000/api/customer/id  

Update Customer  
http://localhost:5000/api/update-customer/id  

Delete Customer  
http://localhost:5000/api/delete-customer/id  

## License
MIT