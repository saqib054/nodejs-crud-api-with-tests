const customerController = require('../controllers/customer.controller');
const httpMocks = require('node-mocks-http');
const supertest = require('supertest');
const app = require('../server');
const mongoose = require('mongoose');

let req, res, next;
const newCustomer = { name: 'lorem', bags: '5' };

// Connect to the MongoDB Memory Server
beforeAll(async () => {
  await mongoose.connect(
    global.__MONGO_URI__,
    { useNewUrlParser: true, useCreateIndex: true },
    err => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    }
  );
});

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = jest.fn();
});

describe('customer controller', () => {
  it('should have addCustomer function defined', () => {
    expect(typeof customerController.addCustomer).toBeDefined();
  });

  it('should save customer in database', async () => {
    req.body = newCustomer;
    await customerController.addCustomer(req, res, next);
    expect(res._getJSONData()).toStrictEqual({
      message: 'New customer created..!'
    });
    expect(res.statusCode).toBe(201);
    expect(res._isEndCalled()).toBeTruthy();
  });

  it('tests the add-customer route', async () => {
    req.body = newCustomer;
    const response = await supertest(app)
      .post('/api/add-customer')
      .send(newCustomer);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('message');
  });
});
