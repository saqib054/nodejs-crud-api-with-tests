const mongoose = require('mongoose');
const autoIncrement = require('mongoose-sequence')(mongoose);

const CustomerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  bags: {
    type: Number
  }
});

CustomerSchema.plugin(autoIncrement, { inc_field: 'customer_id' });
CustomerSchema.index({ name: 'text' });
const Customer = mongoose.model('Customer', CustomerSchema);
module.exports = Customer;
