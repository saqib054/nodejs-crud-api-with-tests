const express = require('express');
const router = express.Router();

const {
  addCustomer,
  getCustomer,
  getAllCustomers,
  updateCustomer,
  deleteCustomer
} = require('../controllers/customer.controller');

// Create new customer
// @route POST api/add-customer
// @access User
router.post('/add-customer', addCustomer);

// Get one todoTask
// @route GET api/customer/id
// @access Public
router.get('/customer/:id', getCustomer);

// Get all customers
// @route GET api/customer-list
// @access Public
router.get('/customer-list', getAllCustomers);

// Update a customer
// @route PUT api/update-customer
// @access Public
router.put('/update-customer/:id', updateCustomer);

// Delete a customer
// @route DELETE api/delete-customer/id
// @access Public
router.delete('/delete-customer/:id', deleteCustomer);

module.exports = router;
