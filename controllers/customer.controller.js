const Customer = require('../models/customer.model');
const { ErrorHandler } = require('../middleware/error');

exports.addCustomer = async (req, res, next) => {
  try {
    const { name, bags } = req.body;
    if (!name) {
      throw new ErrorHandler(400, 'Name is required');
    }
    const customer = new Customer({
      name,
      bags
    });
    await customer.save();
    res.status(201);
    res.json({ message: 'New customer created..!' });
  } catch (error) {
    next(error);
  }
};

exports.getCustomer = async (req, res, next) => {
  try {
    const queryParams = { _id: req.params.id };
    const customer = await Customer.findOne(queryParams);
    if (!customer) {
      throw new ErrorHandler(404, 'Customer not found');
    }
    res.status(200);
    res.json(customer);
  } catch (error) {
    next(error);
  }
};

exports.getAllCustomers = async (req, res, next) => {
  try {
    const queryParams = {
      pageIndex: parseInt(req.query.pageIndex) || 1,
      size: parseInt(req.query.size) || 5
    };
    const totalCount = await Customer.countDocuments();
    const customers = await Customer.find()
      .skip(queryParams.size * (queryParams.pageIndex - 1))
      .limit(queryParams.size);
    if (!customers) {
      throw new ErrorHandler(404, 'Customers not found');
    }
    res.status(200);
    res.json({ customers, totalCount });
  } catch (error) {
    next(error);
  }
};

exports.updateCustomer = async (req, res, next) => {
  try {
    const queryParams = { _id: req.params.id };
    const customer = await Customer.findOne(queryParams);
    if (!customer) {
      throw new ErrorHandler(404, 'Customer not found');
    }

    const { name, bags } = req.body;
    if (!name) {
      throw new ErrorHandler(400, 'Name is required');
    }
    const updatecustomer = {
      $set: { name, bags }
    };
    await Customer.updateOne(queryParams, updatecustomer);
    res.status(200);
    res.json({ message: 'Customer updated..!' });
  } catch (error) {
    next(error);
  }
};

exports.deleteCustomer = async (req, res, next) => {
  try {
    const queryParams = { _id: req.params.id };
    const customer = await Customer.findOne(queryParams);
    if (!customer) {
      throw new ErrorHandler(404, 'Customer not found');
    }
    await customer.deleteOne(queryParams);
    res.status(200);
    res.json({ message: 'Customer deleted..!' });
  } catch (error) {
    next(error);
  }
};
